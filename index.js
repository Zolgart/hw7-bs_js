/* 
 Теоретичні питання
 1. Як можна створити рядок у JavaScript?
 2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?
 3. Як перевірити, чи два рядки рівні між собою?
 4. Що повертає Date.now()?
 5. Чим відрізняється Date.now() від new Date()?

Практичні завдання 
1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом 
(читається однаково зліва направо і справа наліво), або false в іншому випадку.

2. Створіть функцію, яка  перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити, максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший. Ця функція стане в нагоді для валідації форми. Приклади використання функції:
// Рядок коротше 20 символів
funcName('checked string', 20); // true
// Довжина рядка дорівнює 18 символів
funcName('checked string', 10); // false

3. Створіть функцію, яка визначає скільки повних років користувачу. 
   Отримайте дату народження користувача через prompt. 
   Функція повина повертати значення повних років на дату виклику функцію.
*/



/*
1)Рядок в JavaScript можна створити, використовуючи одинарні (''), подвійні ("") лапки або зворотні (```) лапки (шаблонні рядки).

2)Одинарні та подвійні лапки: Мають однаковий ефект і можуть використовуватися для визначення рядків. Різниця в тому, які лапки виберете.
Шаблонні рядки (```): Дозволяють вставляти значення змінних без конкатенації та використовувати вирази.

3)Для перевірки рівності рядків можна використовувати оператор === або ==. Оператор === порівнює значення та тип даних, тоді як == може перетворювати типи.

4)Date.now() повертає кількість мілісекунд, які минули з 1 січня 1970 року (Епоха) до моменту виклику функції.

5)Date.now() повертає кількість мілісекунд від Епохи і є числовим значенням.
new Date() створює новий об'єкт дати, який містить поточну дату та час.
*/



// 1
function isPalindrome(str) {
    let reversedStr = str.split('').reverse().join('');
    return str === reversedStr;
}

console.log(isPalindrome('level')); 
console.log(isPalindrome('hello')); 




// 2
function checkStringLength(str, maxLength) {
    return str.length <= maxLength;
}

console.log(checkStringLength('checked string', 20)); 
console.log(checkStringLength('checked string', 10)); 



// 3
function calculateAge() {
    const birthDate = prompt("Введіть вашу дату народження (рррр-мм-дд):");
    let today = new Date();
    let birthYear = new Date(birthDate).getFullYear();
    let age = today.getFullYear() - birthYear;

    if (today.getMonth() < new Date(birthDate).getMonth() ||
        (today.getMonth() === new Date(birthDate).getMonth() && today.getDate() < new Date(birthDate).getDate())) {
        age--;
    }

    return age;
}


console.log("Вам " + calculateAge() + " років.");
